package model;

import java.util.ArrayList;

public class ControleDisciplina{
	// Atributos
	private ArrayList<Disciplina> listaDisciplina;

	//Construtor
	public ControleDisciplina(){
		listaDisciplina = new ArrayList<Disciplina>();
	}

	// metodos

	public void adicionar(Disciplina umaDisciplina){
		listaDisciplina.add(umaDisciplina);
	}

	public void remover(Disciplina umaDisciplina){
		listaDisciplina.remove(umaDisciplina);
	}


}
