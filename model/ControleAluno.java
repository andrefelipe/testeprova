package model;

import java.util.ArrayList;

public class ControleAluno{
	// Atributos
	private ArrayList<Aluno> listaAluno;
	//Construtor
	public ControleAluno(){
		listaAluno = new ArrayList<Aluno>();
	}

	// metodos

	public void adicionar(Aluno umAluno){
		listaAluno.add(umAluno);
	}

	public void remover(Aluno umAluno){
		listaAluno.remove(umAluno);
	}


}
