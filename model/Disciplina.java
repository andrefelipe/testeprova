package model;

import java.util.ArrayList;

public class Disciplina{

	//Atributos
	private String nomeDisciplina;
	private String professor;
	private ArrayList<Aluno> listaAlunos;

	//Construtor
	public Disciplina(){
		listaAlunos = new ArrayList<Aluno>();
	}
	
	//Metodos	
	public void setNomeDisciplina(String umNomeDisciplina){
		nomeDisciplina = umNomeDisciplina;
	}

	public String getNomeDisciplina(){
		return nomeDisciplina;
	}
	
	public void setProfessor(String umProfessor){
		professor = umProfessor;
	}

	public String getProfessor(){
		return professor;
	}


}
