package model;

import java.io.*;

public class CadastroAlunosDisciplina{

	public static void main (String[] args) throws IOException{
		// entrada do teclado
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;
		char menuOpcao;

		ControleAluno umControleAluno = new ControleAluno();
		ControleDisciplina umControleDisciplina = new ControleDisciplina();
		
		
			do{
				// imprime o menu na tela
				System.out.println("Menu");
				System.out.println("1-Cadastrar aluno");
				System.out.println("2-Cadastrar disciplina");
				System.out.println("3-Remover aluno");				
				System.out.println("4-Remover disciplina");		
				System.out.println("0-Sair");	

				System.out.print("Insira a opção: ");	
				entradaTeclado = leitorEntrada.readLine();
				menuOpcao = entradaTeclado.charAt(0);

				switch (menuOpcao){
					case '1':
						System.out.print("Digite o nome do aluno: ");
						entradaTeclado = leitorEntrada.readLine();
						String umNome = entradaTeclado;

						System.out.print("Digite a matrícula: ");
						entradaTeclado = leitorEntrada.readLine();
						String umaMatricula = entradaTeclado;

						Aluno umAluno = new Aluno(umNome, umaMatricula);
						break;
					case '2':
						System.out.print("Digite o nome da disciplina: ");
						entradaTeclado = leitorEntrada.readLine();
						String umNomeDisciplina = entradaTeclado;

						System.out.print("Digite o nome do professor: ");
						entradaTeclado = leitorEntrada.readLine();
						String umProfessor = entradaTeclado;

						Disciplina umaDisciplina = new Disciplina();
						break;
					
				}// fim do switch
					
			}while(menuOpcao != '0');
	} // fim da main

}// fim do cadastroAlunos
