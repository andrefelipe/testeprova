package model;

public class Aluno{
	String nome;
	String matricula;

	// Metodos
	public Aluno(){
		
	}
	
	public Aluno(String umNome, String umaMatricula){
		nome = umNome;
		matricula = umaMatricula;
	}

	public void setNome(String umNome){
		nome = umNome;
	}
	
	public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
	}

	public String getNome(){
		return nome;
	}
	public String getMatricula(){
		return matricula;
	}

}
