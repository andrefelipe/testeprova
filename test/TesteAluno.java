package test;

import static org.junit.Assert.*;

import model.Aluno;

import org.junit.Before;
import org.junit.Test;

public class TesteAluno {
	
	private Aluno testeAluno;
	
	@Before
	public void setUp() throws Exception{
		testeAluno = new Aluno();
	}

	@Test
	public void setNomeTest() {
		testeAluno.setNome("André");
		assertEquals("André" ,testeAluno.getNome());
	}
	
	@Test
	public void setMatriculaTest() {
		testeAluno.setMatricula("1234");
		assertEquals("1234" ,testeAluno.getMatricula());
	}

}
