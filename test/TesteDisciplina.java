package test;

import static org.junit.Assert.*;

import model.Aluno;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class TesteDisciplina {

	private Disciplina testeDisciplina;
	
	@Before
	public void setUp() throws Exception{
		testeDisciplina = new Disciplina();
	}

	@Test
	public void setNomeDisciplinaTest() {
		testeDisciplina.setNomeDisciplina("OO");
		assertEquals("OO" ,testeDisciplina.getNomeDisciplina());
	}
	

	@Test
	public void setProfessorTest() {
		testeDisciplina.setProfessor("Paulo");
		assertEquals("Paulo" ,testeDisciplina.getProfessor());
	}

}
